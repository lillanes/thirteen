Thirteen is a simple (but hard) solitaire card game. 

I plan to implement the game in Haskell, as a means to learn the language. As 
of now I'm working on a naive implementation (no monads, with state passed 
from function to function).

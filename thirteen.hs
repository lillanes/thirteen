import Data.List
import System.Random
import Data.Array.IO
import Control.Monad
import Data.Char
import Data.Array.IO
import Control.Monad
import Control.Monad.ST
import Data.STRef
import Data.Array.ST
 

data Color = Red
           | Black
             deriving (Eq,Show,Bounded)

data Suit = Clubs
          | Diamonds
          | Hearts
          | Spades
            deriving (Eq,Enum,Show,Bounded)

data Value = Ace
           | Two
           | Three
           | Four
           | Five
           | Six
           | Seven
           | Eight
           | Nine
           | Ten
           | Jack
           | Queen
           | King
             deriving (Eq,Enum,Show,Bounded)

data Card = Card Value Suit Bool
            deriving (Eq,Show)

type State = (Deck, [Column])

type Column = [Card]
type Deck = [Card]

type Position = (Int, Int)

deck :: [Card]
deck = [Card x y True| x <- [Ace .. King], y <- [Clubs .. Spades] ]

twoDecks :: [Card]
twoDecks = deck ++ deck

testScale :: [Card]
testScale = reverse (concat (transpose [[Card x Clubs True | x <- [Ace, Three .. King]], [Card x Hearts True | x <- [Two, Four .. Queen]]]))

testColumns :: [Column]
testColumns = [[],[],[],[],[],[],[],[],[],[],[],[],[]]

testRow :: [Card]
testRow = take 13 deck

suitColor :: Suit -> Color
suitColor suit = if suit == Clubs || suit == Spades
                 then Black
                 else Red

color :: Card -> Color
color (Card _ suit _) = suitColor suit

value :: Card -> Value
value (Card value _ _) = value

suit :: Card -> Suit
suit (Card _ suit _) = suit

visible :: Card -> Bool
visible (Card _ _ bool) = bool

follows :: Card -> Card -> Bool
follows (Card King _ _) _ = False
follows (Card top_value _ _) (Card bottom_value _ _) = (succ top_value) == bottom_value

isEmptyColumn :: [Column] -> Position -> Bool
isEmptyColumn columns (column, depth) = (depth == 0) && (length (columns !! column) == 0)

placeableOnCard :: Card -> Card -> Bool
placeableOnCard top bottom = (follows top bottom) && (color top) /= (color bottom)

placeable :: [Column] -> Card -> Position -> Bool
placeable columns top bottom_target = 
    if (value top) == King
    then isEmptyColumn columns bottom_target
    else if (snd bottom_target) == 0
         then False
         else placeableOnCard top (getCardBefore columns bottom_target)

moveable :: [Card] -> Bool
moveable [(Card _ _ _)] = True
moveable cards = (moveable (tail cards)) && (placeableOnCard (head (tail cards)) (head cards))

valid :: Position -> Bool
valid (column, depth) = not (column < 0 || column > 12 || depth < 0)

exists :: [Column] -> Position -> Bool
exists columns (column, depth) = valid (column, depth) && (length (columns !! column) > depth)

available :: [Column] -> Position -> Bool
available columns (column, 0) = valid (column, 0) && (length (columns !! column) == 0)
available columns (column, depth) = (exists columns (column, (depth - 1))) && (length (columns !! column) == depth)

getCard :: [Column] -> Position -> Card
getCard columns (column, depth) = (columns !! column) !! depth

getCardBefore :: [Column] -> Position -> Card
getCardBefore columns (column, depth) = (columns !! column) !! (depth - 1)

getStack :: [Column] -> Position -> [Card]
getStack columns (column, depth) = (drop depth (columns !! column)) 

stack :: [Column] -> Position -> Position -> [Column]
stack columns source (column, depth) = 
    tryRemove 
    ((take column columns) ++ [(columns !! column) ++ (getStack columns source)] ++ (drop (column + 1) columns))
    (column, depth)

unstack :: [Column] -> Position -> [Column]
unstack columns (column, depth) = 
    (take column columns) 
    ++ [setLastCardVisible (take depth (columns !! column))]
    ++ (drop (column + 1) columns)

put :: [Column] -> Position -> Position -> [Column]
put columns source target = (unstack (stack columns source target) source)

move :: [Column] -> Position -> Position -> [Column]
move columns source target = 
    if not (exists columns source)
    then columns
    else if (moveable (getStack columns source)) && (available columns target)
         then if (placeable columns (getCard columns source) target)
              then (put columns source target)
              else columns
         else columns

complete :: [Card] -> Bool
complete cards = (moveable cards) && (value (head cards) == King) && ((length cards) == 13)

tryRemove :: [Column] -> Position -> [Column]
tryRemove columns (column, depth) =
    if (value (getCard columns (column, depth))) == King
    then remove columns (column, depth)
    else if depth > 0
         then tryRemove columns (column, (depth-1))
         else columns

remove :: [Column] -> Position -> [Column]
remove columns position =
    if not (exists columns position)
    then columns
    else if (complete (getStack columns position))
         then (unstack columns position)
         else columns

--shuffle :: Int -> Deck -> Deck
--shuffle index cards = (permutations cards) !! index
--
--start :: State
--start = deal (shuffle 876537642 ((shuffle 34876234 deck) ++ (shuffle 76512213 deck)))

deal :: Deck -> State
deal cards = dealRow (dealInvisibleRow (dealRow (dealInvisibleRow (cards, testColumns))))

dealRow :: State -> State
dealRow (cards, columns) = ((drop 13 cards), (dealFixedRow (take 13 cards) columns))

dealInvisibleRow :: State -> State
dealInvisibleRow (cards, columns) = ((drop 13 cards), (dealFixedRow (map setInvisible (take 13 cards)) columns))

setInvisible :: Card -> Card
setInvisible (Card value suit _) = (Card value suit False)

setVisible :: Card -> Card
setVisible (Card value suit _) = (Card value suit True)

setLastCardVisible :: [Card] -> [Card]
setLastCardVisible [] = []
setLastCardVisible cards = (take ((length cards) - 1) cards) ++ [setVisible (cards !! ((length cards) - 1))]

dealFixedRow :: [Card] -> [Column] -> [Column]
dealFixedRow cards columns = map appendItem (zip cards columns)

appendItem :: (a, [a]) -> [a]
appendItem (card, column) = column ++ [card]

valueToChar value = if value == Ace || value == Jack || value == Queen || value == King || value == Ten
                    then (show value) !! 0
                    else chr ((fromEnum value) + 49)

suitToChar suit = (show suit) !! 0

visibleCardToString card = [valueToChar (value card), suitToChar (suit card)] 

cardToString card = if (visible card)
                    then visibleCardToString card
                    else "##"

columnToString column = map cardToString column

columnsToString columns = concat (intersperse "\n" (map concat (map (\x -> intersperse " " x) (stringTranspose (map columnToString columns)))))

extend :: Int -> [[Char]] -> [[Char]]
extend size column = if (length column) < size
                     then extend size (column ++ ["  "])
                     else column

stringTranspose :: [[[Char]]] -> [[[Char]]]
stringTranspose matrix = transpose (map (\x -> extend (maximum (map length matrix)) x) matrix)

printState (cards, columns) = 
    do
    putStrLn (columnsToString columns)
    putStrLn "\n"
    putStr "Deal? (y/N) "
    inpStr <- getLine
    if inpStr == "y"
    then printState (dealRow (cards,columns))
    else chooseStep (cards,columns)

getColumns (cards, columns) = columns

chooseStep (cards, columns) =
    do
    putStr "Choose source column: "
    source_column <- getLine
    putStr "Choose source row: "
    source_row <- getLine
    putStr "Choose target column: "
    target_column <- getLine
    printState (doMove (cards, columns) source_column source_row target_column)

doMove (cards, columns) source_column source_row target_column =
    (cards, move columns (buildSource source_column source_row) (buildTarget columns target_column))

buildSource column row =
    (processInput (reads column :: [(Int,String)]), processInput (reads row :: [(Int,String)]))

buildTarget columns column =
    (processInput (reads column :: [(Int,String)]), buildTargetRow columns (processInput (reads column :: [(Int,String)])))

buildTargetRow columns column =
    if column == -1
    then -1
    else length (columns !! column)

processInput input = if (length input) > 0
                     then fst (input !! 0)
                     else -1

begin = 
    do
    putStr "Seed: "
    seed <- getLine
    printState (deal (fst (shuffle' twoDecks (mkStdGen (read seed :: Int)))))

beginTest =
    printState (deal twoDecks)
    
-- | Randomly shuffle a list without the IO Monad
--   /O(N)/
--   source: http://www.haskell.org/haskellwiki/Random_shuffle
shuffle' :: [a] -> StdGen -> ([a],StdGen)
shuffle' xs gen = runST (do
        g <- newSTRef gen
        let randomRST lohi = do
              (a,s') <- liftM (randomR lohi) (readSTRef g)
              writeSTRef g s'
              return a
        ar <- newArray n xs
        xs' <- forM [1..n] $ \i -> do
                j <- randomRST (i,n)
                vi <- readArray ar i
                vj <- readArray ar j
                writeArray ar j vi
                return vj
        gen' <- readSTRef g
        return (xs',gen'))
  where
    n = length xs
    newArray :: Int -> [a] -> ST s (STArray s Int a)
    newArray n xs =  newListArray (1,n) xs
